import asyncio
from examples.api.oidc import request_access_token


async def main():
    token = await request_access_token()
    print(f"access token: {token}")


if __name__ == "__main__":
    asyncio.run(main())
