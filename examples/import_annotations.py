import asyncio
import sys
import os
import xml.etree.ElementTree as et
from gql import gql
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session

data_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..\\data')

CLASSIFIER_NAME = None


async def example(session, image_pk):
    # -----------------
    # Query Image By Pk
    # -----------------
    image = await get_image(session, image_pk)

    # ----------------------------------------
    # Create XML string form .annotations file
    # ----------------------------------------
    xml = et.parse(f'{data_path}\\test_layer.annotations').getroot()
    xml_str = et.tostring(xml).decode(encoding="utf-8")

    # --------------------------
    # Import Annotation to Image
    # --------------------------
    annotation = await import_annotations(session, image["id"], xml_str)
    print(f'created annotations: {annotation}')

    if CLASSIFIER_NAME is not None:
        # ------------------------
        # Query Classifier by Name
        # ------------------------
        classifier = await get_classifier(session, CLASSIFIER_NAME)
        print(classifier)

        # ---------------------
        # Create Training Class
        # ---------------------
        training_class = await create_training_class(session, classifier, annotation)
        print(training_class)

        # ---------------------------------
        # Copy Annotation to Training Class
        # ---------------------------------
        training_class = await copy_annotation_to_training_class(session, training_class, annotation)
        print(training_class)

        # --------------------------------
        # Delete Original Image Annotation
        # --------------------------------
        await delete_annotation(session, annotation)


async def get_image(session, image_pk):
    obj = await session.execute(
        gql("""
            query test($pk: Int!) {
              imageByPk(pk:$pk) {
                pk
                id
                location
              }
            }
        """),
        variable_values={"pk": image_pk}
    )
    return obj["imageByPk"]


async def import_annotations(session, image_id, xml_string):
    obj = await session.execute(
        gql("""
            mutation($input: ImportAnnotationsInput!) {
              importAnnotations(input: $input) {
                mutated {
                  node {
                    pk
                    id
                    color
                    name
                  }
                }
              }
            }
        """),
        variable_values={
            "input": {
                "value": xml_string,
                "imageId": image_id,
                "format": "APERIO_XML"
            }
        }
    )
    return obj["importAnnotations"]["mutated"][0]["node"]


async def get_classifier(session, class_name):
    obj = await session.execute(
        gql("""
            query($name: String!) {
              classifiers(
                query: {
                  where: {
                    q: [
                      { f: "name", pred: [{ op: EQ, v: $name }] }
                    ]
                  }
                }
              ) {
                edges {
                  node {
                    pk
                    id
                    name
                    classes {
                      pk
                      id
                      name
                    }
                  }
                }
              }
            }
        """),
        variable_values={"name": class_name}
    )
    return obj["classifiers"]["edges"][0]["node"]


async def create_training_class(session, classifier, annotation):
    obj = await session.execute(
        gql("""
            mutation($input: CreateTrainingClassInput!) {
              createTrainingClass(input: $input) {
                mutated {
                  node {
                    pk
                    id
                    name
                  }
                }
              }
            }
        """),
        variable_values={
            "input": {
                "classifierId": classifier["id"],
                "name": annotation["name"],
                "color": annotation["color"]
            }
        }
    )
    return obj["createTrainingClass"]["mutated"][0]["node"]


async def copy_annotation_to_training_class(session, training_class, annotation):
    obj = await session.execute(
        gql("""
            mutation($input: CopyAnnotationsToTrainingClassesInput!) {
              copyAnnotationsToTrainingClasses(input: $input) {
                mutated {
                  node {
                    pk
                    id
                    name
                  }
                }
              }
            }
        """),
        variable_values={
            "input": {
                "inputs": [
                    {
                        "sourceAnnotationLayerId": annotation["id"],
                        "targetTrainingClassId": training_class["id"]
                    }
                ]
            }
        }
    )
    return obj["copyAnnotationsToTrainingClasses"]["mutated"][0]["node"]


async def delete_annotation(session, annotation):
    await session.execute(
        gql("""
            mutation($input: DeleteAnnotationLayerInput!) {
              deleteAnnotationLayer(input: $input) {
                deleted
              }
            }
        """),
        variable_values={
            "input": {
                "layerId": annotation["id"]
            }
        }
    )


async def main(image_pk):
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session, image_pk)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(main(int(sys.argv[1])))
