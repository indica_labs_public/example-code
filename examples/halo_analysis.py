import asyncio
import sys
from gql import gql
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session


async def example(session, image_pk, setting_name):
    # -----------------
    # Query Image By Pk
    # -----------------
    image = await session.execute(
        gql("""
            query test($pk: Int!) {
              imageByPk(pk:$pk) {
                id
                location
              }
            }
        """),
        variable_values={"pk": image_pk}
    )
    print(f'found image: {image["imageByPk"]["location"]} ({image["imageByPk"]["id"]})')
    image_id = image["imageByPk"]["id"]

    # -------------------------------
    # Query Analysis Settings By Name
    # -------------------------------
    # NOTE: Use 'classifiers' query
    #  to run analysis as well.
    settings = await session.execute(
        gql("""
            query($setting_name: String!) {
              settings(
                query: {
                  where: {
                    q: {
                      f: "savedName"
                      pred: {
                        op: EQ
                        v: $setting_name
                      }
                    }
                  }
                }
              ) {
                edges {
                  node {
                    pk
                    id
                    algorithmName
                    savedName
                  }
                }
              }
            }
        """),
        variable_values={"setting_name": setting_name}
    )
    print(f'found settings: {settings["settings"]["edges"][0]["node"]["savedName"]} ({settings["settings"]["edges"][0]["node"]["id"]})')
    settings_id = settings["settings"]["edges"][0]["node"]["id"]

    # ---------------------
    # Schedule Analysis Job
    # ---------------------
    scheduled_job = await session.execute(
        gql("""
            mutation($settingsId: ID!, $regionDescription: [ImageAnalysisDescription]!) {
              scheduleAnalysis(
                input: { settingsId: $settingsId, analysisDescriptions: $regionDescription }
              ) {
                mutated {
                  node {
                    id
                    pk
                    settingsSavedName
                    image {
                      id
                      pk
                    }
                  }
                }
              }
            }
        """),
        variable_values={
            "settingsId": settings_id,
            "regionDescription": [
                {
                    "imageId": image_id,
                    "fieldOfView": None
                }
            ]
        }
    )
    print(f'scheduled analysis: {scheduled_job["scheduleAnalysis"]["mutated"][0]["node"]["settingsSavedName"]} on image ({scheduled_job["scheduleAnalysis"]["mutated"][0]["node"]["image"]["pk"]})')
    job_id = scheduled_job["scheduleAnalysis"]["mutated"][0]["node"]["id"]

    # ------------------
    # Query Job Progress
    # ------------------
    while True:
        job = await session.execute(
            gql("""
                query($job_id: ID!) {
                  jobById(id: $job_id) {
                    pk
                    id
                    settingsSavedName
                    progress {
                      condition
                      percentComplete
                      errorMessage
                    }
                  }
                }
            """),
            variable_values={
                "job_id": job_id
            }
        )
        if job["jobById"]["progress"]["condition"] == "FAILED":
            error_message = job["jobById"]["progress"]["errorMessage"]
            print(f'job error: {error_message}')
            break
        elif job["jobById"]["progress"]["condition"] == "CANCELLED":
            print("reported progress: CANCELLED")
            break
        elif job["jobById"]["progress"]["condition"] == "COMPLETE":
            print("reported progress: 100%")
            break
        else:
            progress = job["jobById"]["progress"]["percentComplete"] or 0
            print(f'reported progress: {int(progress)}%')
        await asyncio.sleep(2)


async def main(image_pk, setting_name):
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session, image_pk, setting_name)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(main(int(sys.argv[1]), str(sys.argv[2])))
