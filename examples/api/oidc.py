import aiohttp
import ssl
from config.config import get_settings

settings = get_settings()

# -----------------------------------------------
# Setup connection settings for Identity Provider
# -----------------------------------------------
HOSTNAME = settings["authorization"]["hostname"]
CLIENT_ID = settings["authorization"]["client_id"]
CLIENT_SECRET = settings["authorization"]["client_secret"]


async def request_access_token():
    # ------------------
    # Fetch Access Token
    # ------------------
    async with aiohttp.ClientSession() as session:
        async with session.request(
            method="post",
            url=f"https://{HOSTNAME}/idsrv/connect/token",
            data={
                "client_id": CLIENT_ID,
                "client_secret": CLIENT_SECRET,
                "scope": "serviceuser graphql",
                "grant_type": "client_credentials"
            },
            ssl=ssl.SSLContext(ssl.PROTOCOL_TLS),
            raise_for_status=True
        ) as response:
            data = await response.json()
        return data['access_token']
