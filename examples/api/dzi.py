import math
import requests
import xml.etree.ElementTree as et
from config.config import get_settings

settings = get_settings()

HOSTNAME = settings["authorization"]["hostname"]

# Image Server API Interface
def deep_zoom_image(url_path: str, authz_token: str):
    url = f"https://{HOSTNAME}/image/{url_path}"
    response = requests.get(
        url=url + f"?authzToken={authz_token}",
        verify=False
    )
    response.raise_for_status()
    return response

# Image Server API Interface
def deep_zoom_dimensions(deep_zoom_image: str):
    image = et.fromstring(deep_zoom_image)
    size = image.find('{http://schemas.microsoft.com/deepzoom/2008}Size').attrib
    width = int(size['Width'])
    height = int(size['Height'])
    tile_size = int(image.attrib['TileSize'])
    px = max(width, height)
    max_level = math.ceil(math.log2(px))
    min_level = math.ceil(math.log2(tile_size))
    dimensions = {
        "width": width,
        "height": height,
        "tile_size": tile_size,
        "max_level": max_level,
        "max_tiles_across": math.ceil(width / tile_size),
        "max_tiles_down": math.ceil(height / tile_size),
        "min_level": min_level,
        "min_tiles_across": math.ceil(2 ** min_level / tile_size),
        "min_tiles_down": math.ceil(2 ** min_level / tile_size)
    }
    return dimensions
