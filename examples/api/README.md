# Example Code Overview

## [API Client](./client.py)

This has the **create_client_session** function that creates a GraphQL Client and returns the client session object. It uses the python [gql](https://gql.readthedocs.io/en/stable/) library to create the client.

## [Client Credentials Flow](./oidc.py)

This example code demonstrates authentication using Client Credentials Flow to fetch and **access token**.

## [Authorization Code Flow](./local.py)

This example code demonstrates authentication using Authorization Code Flow to fetch and **access token**.
