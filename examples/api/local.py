import random
import string
import requests
import socket
import html
import base64
import hashlib
import urllib.parse
import os
import re

# -----------------------------------------------
# Setup connection settings for Identity Provider
# -----------------------------------------------
HOSTNAME = ""
USERNAME = ""
PASSWORD = ""

CERTIFICATE_PEM = None # r"C:\ProgramData\Indica Labs\Configuration\IndicaLabs.ApplicationLayer.SharedConfiguration\ca.snakeoil.crt"


def request_access_token():
    # Create requests session
    session = requests.Session()
    certificate_pem = CERTIFICATE_PEM or False

    # Finds unbound port for OIDC redirect
    sock = socket.socket()
    sock.bind(("", 0))
    port = sock.getsockname()[1]

    # Pre installed OIDC variables
    client_id = "halo"
    redirect_uri = f"http://127.0.0.1:{port}"

    # PKCE code verifier and challenge
    code_verifier = base64.urlsafe_b64encode(os.urandom(40)).decode("utf-8")
    code_verifier = re.sub("[^a-zA-Z0-9]+", "", code_verifier)
    code_challenge = hashlib.sha256(code_verifier.encode("utf-8")).digest()
    code_challenge = base64.urlsafe_b64encode(code_challenge).decode("utf-8")
    code_challenge = code_challenge.replace("=", "")

    # Request login page
    state = "".join(random.choice(string.printable) for i in range(30))
    request = requests.Request(
        method="GET",
        url=f"https://{HOSTNAME}/idsrv/connect/authorize",
        params={
            "client_id": client_id,
            "redirect_uri": redirect_uri,
            "response_type": "code",
            "scope": "openid profile graphql",
            "state": state,
            "code_challenge": code_challenge,
            "code_challenge_method": "S256"
        }
    )
    prepped = session.prepare_request(request)
    response1 = session.send(
        prepped,
        allow_redirects=False,
        verify=certificate_pem
    )
    response1_1 = session.send(
        response1.next,
        verify=certificate_pem
    )

    # Parse login page for request verification token
    page = response1_1.text
    request_token = html.unescape(re.search('<input\s+.*?\s+value="(.*?)"', page, re.DOTALL).group(1))

    # Parse response header for anti-forgery token
    cookie = response1_1.headers["Set-Cookie"]
    cookie = "; ".join(c.split(";")[0] for c in cookie.split(", "))

    # Send user login credentials
    request2 = requests.Request(
        method="POST",
        url=response1_1.url,
        data={
            "__RequestVerificationToken": request_token,
            "scheme": "local",
            "username": USERNAME,
            "password": PASSWORD,
        },
        headers={"Cookie": cookie}
    )
    prepped2 = session.prepare_request(request2)
    response2 = session.send(
        prepped2,
        allow_redirects=False,
        verify=certificate_pem
    )

    if response2.is_redirect:
        response2_1 = session.send(
            response2.next,
            allow_redirects=False,
            verify=certificate_pem
        )
    else:
        raise Exception(f"The password for user '{USERNAME}' was invalid.")

    # Parse redirect url for authentication code
    redirect = response2_1.next.url
    query = urllib.parse.urlparse(redirect).query
    redirect_params = urllib.parse.parse_qs(query)
    auth_code = redirect_params["code"][0]

    # Exchange authorization code for an access token
    request3 = requests.Request(
        method="POST",
        url=f"https://{HOSTNAME}/idsrv/connect/token",
        data={
            "client_id": "halo",
            "redirect_uri": redirect_uri,
            "code": auth_code,
            "code_verifier": code_verifier,
            "grant_type": "authorization_code",
        }
    )
    prepped3 = session.prepare_request(request3)
    response3 = session.send(
        prepped3,
        allow_redirects=False,
        verify=certificate_pem
    )
    response3.raise_for_status()

    # Return access token
    return response3.json()['access_token']
