import ssl
from gql import Client
from gql.transport.websockets import WebsocketsTransport
from config.config import get_settings

settings = get_settings()

HOSTNAME = settings["authorization"]["hostname"]


# HALO GraphQL API Client
async def create_client_session(token: str, add_local_bearer=False):
    # ------------------------
    # Create GraphQL Transport
    # ------------------------
    transport = WebsocketsTransport(
        url=f"wss://{HOSTNAME}/graphql",
        headers={"authorization": f"bearer {token}"},
        subprotocols=[WebsocketsTransport.APOLLO_SUBPROTOCOL],
        ssl=ssl.SSLContext(ssl.PROTOCOL_TLS)
    )
    if add_local_bearer:
        transport.headers["x-authentication-scheme"] = "LocalBearer"

    # -----------------------------
    # Create GraphQL Client Session
    # -----------------------------
    client = Client(transport=transport)
    return await client.connect_async()
