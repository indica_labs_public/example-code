import asyncio
import os
import sys
from gql import gql
from urllib import parse as urlparse
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session
from examples.api.dzi import deep_zoom_image, deep_zoom_dimensions
import data.completed_job_data as completed_job_data

ANALYSIS_SETTINGS_NAME = "Shared Classifier"
ANALYSIS_VENDOR_NAME = "External Vendor"


async def example(session, image_pk, markup_location):
    # Get image from pk
    image = await get_image(session, image_pk)

    # Schedule analysis job
    job = await schedule_analysis(session, image)
    job_id = job["id"]
    job_pk = job["pk"]
    print(f'schedule analysis job: {job_pk}')

    # Let's HALO know that image is being analysed
    await claim_job(session, job_id)

    # Send progress updates to HALO about job status
    async for progress in progress_updates(session, job):
        print(f"reported progress: {progress}%")

    # Update analysis job results and markup info
    completed_job = await complete_analysis(session, image, job, markup_location)
    print(f"reported progress: 100%")
    print(f"completed job at {completed_job['progress']['endTime']}")


async def get_image(session, image_pk):
    image = await session.execute(
        gql("""
            query($pk: Int!) {
              imageByPk(pk:$pk) {
                id
                location
                token
              }
            }"""),
        variable_values={"pk": image_pk}
    )
    return image["imageByPk"]


async def schedule_analysis(session, image):
    job_result = await session.execute(
        gql("""
            mutation($imageId: ID!, $settingsName: String!, $vendorName: String!) {
              scheduleAdHocAnalysis(
                input: {
                  settings: {
                    external: {
                      name: $settingsName
                      vendor: $vendorName
                    }
                  }
                  analysisDescriptions: { imageId: $imageId }
                }
              ) {
                mutated {
                  node {
                    id
                    pk
                  }
                }
              }
            }"""),
        variable_values={
            "imageId": image["id"],
            "settingsName": ANALYSIS_SETTINGS_NAME,
            "vendorName": ANALYSIS_VENDOR_NAME
        }
    )
    return job_result["scheduleAdHocAnalysis"]["mutated"][0]["node"]


async def claim_job(session, job_id):
    await session.execute(
        gql("""
            mutation($jobId: ID!, $analysisEngineVersion: String!) {
              claimJob(input: {
                jobId: $jobId
                analysisEngineVersion: $analysisEngineVersion
              }) {
                mutated {
                  node {
                    id
                    pk
                  }
                }
              }
            }"""),
        variable_values={"jobId": job_id, "analysisEngineVersion": "1.0"},
    )


async def progress_updates(session, job):
    for i in range(4):
        await session.execute(
            gql("""
                mutation($jobId: ID!, $percentComplete: Float!) {
                  reportProgress(input: {
                    id: $jobId,
                    percentComplete: $percentComplete,
                  })
                }"""),
            variable_values={"jobId": job["id"], "percentComplete": i * 25},
        )
        yield i * 25


async def complete_analysis(session, image, job, markup_location):
    file_name = os.path.splitext(os.path.basename(image["location"]))[0]

    # get dimension
    dzi_url_path = f"{urlparse.quote(image['location'])}.dzi"
    dzi = deep_zoom_image(dzi_url_path, image['token'])
    dimensions = deep_zoom_dimensions(dzi.text)

    # complete job
    job_result = await session.execute(
        gql("""
            mutation($input: CompleteAnalysisInput!) {
              completeAnalysis(input: $input) {
                mutated {
                  node {
                    id
                    progress {
                      endTime
                    }
                  }
                }
              }
            }"""),
        variable_values={
            "input": {
                "jobId": job["id"],
                "bounds": {
                    "top": 0,
                    "left": 0,
                    "width": dimensions['width'],
                    "height": dimensions['height']
                },
                "zoom": 1.0,
                "classifierZoom": 1.0,
                "results": completed_job_data.results,
                "markups": [
                    {
                        "markupLocation": markup_location,
                        "markupImageType": "CLASSIFIER_MARKUP",
                        "bounds": {
                            "top": 0,
                            "left": 0,
                            "width": dimensions['width'],
                            "height": dimensions['height']
                        },
                        "zoom": 1.0,
                    }
                ]
            }
        },
    )
    return job_result["completeAnalysis"]["mutated"][0]["node"]


async def main(image_pk, markup_location):
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session, image_pk, markup_location)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(main(int(sys.argv[1]), sys.argv[2]))
