import asyncio
import os
import sys
from urllib import parse as urlparse
from gql import gql
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session
from examples.api.dzi import deep_zoom_image, deep_zoom_dimensions

TILE_PATH = "8/0_0.jpeg"

output_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..\\output')


async def example(session, image_pk):
    # Get image from pk
    image = await get_image(session, image_pk)
    print(f'found image: \"{image["location"]}\" ({image["id"]})')

    # Fetch deep zoom image from Image Server API
    dzi_url_path = f"{urlparse.quote(image['location'])}.dzi"
    dzi = deep_zoom_image(dzi_url_path, image['token'])
    dimensions = deep_zoom_dimensions(dzi.text)
    print(f'dimensions: {dimensions}')
    print(f'max DeepZoom level: {dimensions["max_level"]}, {dimensions["max_tiles_across"]} by {dimensions["max_tiles_down"]} tiles')
    print(f'min DeepZoom level: {dimensions["min_level"]}, {dimensions["min_tiles_across"]} by {dimensions["min_tiles_down"]} tiles')

    # Fetch deep zoom tile from Image Server API
    tile_url_path = f"{urlparse.quote(image['location'])}_files/" + TILE_PATH
    tile = deep_zoom_image(tile_url_path, image['token'])

    # Write output file
    out_file_name = f"{os.path.basename(image['location']).split('.')[0]}-{'-'.join(TILE_PATH.split('/'))}"
    out_file_path = os.path.abspath(f"{output_path}\\{out_file_name}")
    with open(out_file_path, 'wb') as f:
        for chunk in tile.iter_content():
            f.write(chunk)
    print(f'wrote tile to {out_file_path}')


async def get_image(session, image_pk):
    image = await session.execute(
        gql("""
            query($pk: Int!) {
              imageByPk(pk:$pk) {
                id
                location
                token
              }
            }"""),
        variable_values={"pk": image_pk}
    )
    return image["imageByPk"]

async def main(image_pk):
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session, image_pk)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(main(int(sys.argv[1])))
