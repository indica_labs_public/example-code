import asyncio
import math
import sys
from gql import gql
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session

RADIUS = 5000
ORIGIN = (5000, 5000)


async def example(session, image_pk):
    # Get image id from pk
    image_id = await get_image_id(session, image_pk)

    # Create the annotation layer
    layer_id = await create_annotation_layer(session, image_id)

    # Create region coordinates
    coordinates_string = create_pentagon_geometry()

    # Draw region in annotation lay
    input_gql = {
        "layerId": layer_id,
        "shapeType": "POLYGON",
        "geometry": "{\"type\":\"LineString\",\"coordinates\":" + str(coordinates_string) + "}"
    }
    region = await session.execute(
        gql("""
            mutation($input: DrawAnnotationRegionInput!){
              drawAnnotationRegion(input: $input){
                failed{
                  error
                }
              } 
            }"""),
        variable_values={"input": input_gql}
    )
    failed = region["drawAnnotationRegion"]["failed"]
    if failed is None:
        print("Successfully drawn regions")
    else:
        print("Drawing regions failed: " + failed)


async def get_image_id(session, image_pk):
    image = await session.execute(
        gql("""
            query($pk: Int!) {
              imageByPk(pk:$pk) {
                id
              }
            }
        """),
        variable_values={"pk": image_pk}
    )
    return image["imageByPk"]["id"]


async def create_annotation_layer(session, image_id):
    annotation_layer = await session.execute(
        gql("""
            mutation($input: CreateAnnotationLayerInput!){
              createAnnotationLayer(input: $input){
                mutated{
                  node{
                    id
                    name
                  }
                }
              }
            }
        """),
        variable_values={
            "input": {
                "id": image_id
            }
        }
    )
    return annotation_layer["createAnnotationLayer"]["mutated"][0]["node"]["id"]


def create_pentagon_geometry():
    return "[[{0},{1}], [{2},{3}], [{4},{5}], [{6},{7}], [{8},{9}], [{10},{11}]]"\
        .format(
            str((RADIUS * math.cos(math.radians(90))) + ORIGIN[0]),
            str((-RADIUS * math.sin(math.radians(90))) + ORIGIN[1]),
            str((RADIUS * math.cos(math.radians(18))) + ORIGIN[0]),
            str((-RADIUS * math.sin(math.radians(18))) + ORIGIN[1]),
            str((RADIUS * math.cos(math.radians(306))) + ORIGIN[0]),
            str((-RADIUS * math.sin(math.radians(306))) + ORIGIN[1]),
            str((RADIUS * math.cos(math.radians(234))) + ORIGIN[0]),
            str((-RADIUS * math.sin(math.radians(234))) + ORIGIN[1]),
            str((RADIUS * math.cos(math.radians(162))) + ORIGIN[0]),
            str((-RADIUS * math.sin(math.radians(162))) + ORIGIN[1]),
            str((RADIUS * math.cos(math.radians(90))) + ORIGIN[0]),
            str((-RADIUS * math.sin(math.radians(90))) + ORIGIN[1])
        )

async def main(image_pk):
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session, image_pk)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(main(int(sys.argv[1])))
