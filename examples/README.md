# Example Code Overview

In the python example files, there are variable constants (variable in all *CAPS*) that need to be defined in order to run the example. Open the example python file and define these variables before running the code.

NOTE: The first example code that needs to be run is the **OIDC connection example** [main.py](./main.py). This is because that code is used for authentication in the other examples in this project. By default, all the examples use OIDC for authentication and not the local authentication. Once the authentication example is able to return an **access token**, the remaining example code will be ready to run.

## [Query Image](./examples/query_image.py)

This example queries an image, using the image's integer primary key (image_pk), and returns the image properties that were defined in the GraphQL call to the API.

#### To setup and run

Run the following command.
```cmd
python .\examples\query_image.py <image_pk>
```

## [Subscription Barcode](./examples/subscription_barcode.py)

This example executes a GraphQL subscription for **BarcodeScannedEvent**'s. When this example is run, it won't exit because it is listening for event messages. An event message is sent whenever an image barcode is created/changes. To test this out, run this example and change an image barcode in HALO Link. A new event message will then print in the terminal.

#### To setup and run

Run the following command.
```cmd
python .\examples\subscription_barcode.py
```

## [Deep Zoom Tiles](./examples/deepzoom_tiles.py)

This example acquires tiles from the Indica Labs image server, using the image's integer primary key (image_pk). HALO Link comes installed with an image server which serves whole-slide images using the [Deep Zoom protocol](https://docs.microsoft.com/en-us/previous-versions/windows/silverlight/dotnet-windows-silverlight/cc645050(v=vs.95)), generating tiles on the fly to serve to the web browser. The image server restricts access to pixel data by validating a [JSON Web Token](https://jwt.io) containing the image location. This JWT is signed by the GraphQL API server (after confirming that the calling user is authorized to view the image), and the Indica Labs image server is configured with the verifying key matching the GraphQL server's signing key.

This means that the basic procedure to read image data from the image server is:

- acquire an access token from the Indica Labs identity provider using an OpenID Connect authentication flow

- use this access token to make a request to the GraphQL server to find the desired image, and get an image authorization token

- use the image authorization token and the image location to request a DZI file from the image server, which describes the image dimensions

- use the image authorization token and the image location to request tiles from the image server

The program will write a single tile to `.\output`.

#### To setup and run

For this example, first assign proper values to the following variable constants.
```python
TILE_PATH = None  # Deep zoom tile to retrieve (e.g. "8/0_0.jpeg").
```

Then you can run the following command.
```cmd
python .\examples\deepzoom_tiles.py <image_pk>
```

#### Notes and limitations

You can retrieve either .jpeg or .png tiles by setting the tile path appropriately. .jpeg tiles are lossily compressed and may contain artifacts that make them unsuitable for further analysis, whereas .png tiles are compressed losslessly -- but as a result these will tend to be somewhat larger and slower to create.

Currently, the image server is designed primarily for browser clients such as HALO Link to display images. For brightfield images, PNG tiles at full magnification should contain the exact pixel data from the images. When requesting tiles from a fluorescence image, this is instead a false color display buffer created by the image server using the default channel settings for the image. You can get some idea of the controls that HALO Link uses for adjusting the channel mixing by snooping HALO Link's network traffic as you adjust channel settings:

```
.../15/28_79.jpeg?0=%7B"blackIn"%3A0%2C"brightness"%3A1%2C"colorCode"%3A0%2C"contrast"%3A1%2C"absorption"%3Atrue%2C"gamma"%3A1%2C"whiteIn"%3A1%2C"visible"%3Atrue%7D&1=%7B"blackIn"%3A0%2C"
```

that is, URL-encoded JSON for each channel, with various display controls available, including a hex `colorCode`. However, these tiles are primarily intended for display rather than analysis and we cannot guarantee that the output after this channel mixing process remains 100% true to the original data for each channel.

We are interested in supporting more workflows and formats for allowing third-party software to access image data, if you have ideas or features you would like to see in a future release please don't hesitate to reach out.

## [Custom Analysis](./examples/custom_analysis.py)

This example uses the Indica Labs GraphQL API to create a new analysis job, using the image's integer primary key (image_pk), that won't be managed by the HALO analysis engine -- you control when progress is reported, when the job is considered complete, and which markup image will be used. This can be useful for organizations that want to do their own machine learning tasks on images and then review the results and markup within HALO or HALO Link.

#### To setup and run

For this example, first assign proper values to the following variable constants.
```python
ANALYSIS_SETTINGS_NAME = "Shared Classifier" # Name of the external analysis setting (e.g. "Indica Labs - Area Quantification v2.4.3")
ANALYSIS_VENDOR_NAME = "External Vendor" # Name of the external analysis vendor
```

This script uses the Indica Labs GraphQL API to create a new analysis job that won't be managed by the HALO analysis engine -- you control when progress is reported, when the job is considered complete, and which markup image will be used. This can be useful for organizations that want to do their own machine learning tasks on images and then review the results and markup within HALO or HALO Link.

Then you can run the following command.
```cmd
python .\examples\custom_analysis.py <image_pk> <markup_file_path>
```

## [HALO Analysis](./examples/halo_analysis.py)

Run the following command.
```cmd
python .\examples\halo_analysis.py <image_pk> <setting_name>
```

## [Catalog Image](./examples/catalog_image.py)

This example uses the Indica Labs GraphQL API to a new image file into HALO and adds the image to a study.

#### To setup and run

To catalog the image, the file system path is used (i.e. "_\\10.1.10.211\Andrew\images\svs\114272.svs_"). For this example, the image is also added to a study that MUST already exist. The study pk is used to associate the image to the study.

Then you can run the following command.
```cmd
python .\examples\catalog_image.py <study_pk> <image_path>
```

## [External Script](./examples/external_script.py)

This example leverages Indica Labs GraphQL API to run external scripts from within HALO Link. 

#### To setup and run

To enable external scripts this yaml snippet needs to be added to `C:\ProgramData\Indica Labs\Configuration\IndicaLabs.ApplicationLayer.Halo.GraphQLApi\local-production.yml` and the GraphQL API will need to be restarted.

```yaml
external_scripts:
  scripts:
    Export Annotations:
      executable: ~\Scripts\python.exe # path to the python executable of the project
      working_directory: ~\example-code # working directory of the scripts
      arg_types:
      - label: studyId
        type: Integer
      argv:
      - .\examples\external_script.py # the location of the .py script to run
      description: Export all annotations in the current study     
```
Note: The `working_directory` and the `argv` path to the .py script, when combined should be the exact path of the script.

Once the above configuration has been set up, external scripts will be available to run from HALO Link.

## [Draw Pentagon shaped Annotation](./examples/mutation_pentagonannotation.py)

Example of how to draw a pentagon shaped region using the GraphQL API, using the image's integer primary key (image_pk).

#### To setup and run

For this example, first assign proper values to the following variable constants in [mutation_pentagonannotation.py](./examples/mutation_pentagonannotation.py).

```python
RADIUS = 5000 # Radius of the pentagon
ORIGIN = (5000, 5000) # The center of where the pentagon is to be drawn from
```

Then you can run the following command.
```cmd
python .\examples\mutation_pentagonannotation.py <image_pk>
```

## [Import Annotations](./examples/import_annotations.py)

Example of how to import annotations from a **.annotations** file using the GraphQL API, using the image's integer primary key (image_pk). Then copy the annotations to a Classifier Training Class. 

#### To setup and run

For this example, optionally assign proper values to the following variable constants in [import_annotations.py](./examples/import_annotations.py). If it is not assigned, the annotation will just be imported to the image.

```python
CLASSIFIER_NAME = None
```

Then you can run the following command.
```cmd
python .\examples\import_annotations.py <image_pk>
```

## [Integration Workflow](./examples/integration_workflow.py)

This is an example integration that can be used to update metadata fields, and other data, in HALO Link from 3rd party providers (i.e. LIMS/LIS systems). It creates subscriptions to **imageCatalogedEvent**, **barcodeScannedEvent** and the **imageAddedToStudyEvent** which are used to trigger the integration. These events are fired when an image is cataloged into HALO or HALO Link.

- When the **imageCatalogedEvent** is fired, the example will update an image comment.
- When the **barcodeScannedEvent** is fired, the example will update a image metadata field and the image stain.
- When the **imageCatalogedEvent** is fired, the example will update a study metadata field.

NOTE: These are just examples to help develop an integration and demonstrate how metadata and other data is updated via the GraphQL API when using GraphQL subscriptions. 

#### To setup and run

Before running the example, a **study metadata field** and **image metadata field** must be created in HALO Link. The **study metadata field** can be called "Sample Study Field" and the **image metadata field** can be called "Sample Image Field", both with string field types.

```python
STUDY_FIELD_NAME = "Sample Study Field"
IMAGE_FIELD_NAME = "Sample Image Field"
```

Then you can run the following command.
```cmd
python .\examples\integration_workflow.py
```

This will execute the subscriptions and query the metadata fields that will be used to integrate with 3rd party data.

Once the script is running, catalog an image into HALO and add it to any study. This will fire all the events that the subscriptions are listening on and trigger the integration process.

## [Add a Classifier with XML](./examples/add_classifier.py)

This is an example integration that can be used to add a new classifier to the HALO database using the `addClassifierWithXml` GraphQL mutation. 

#### To setup and run

To add the classifier, the file system path is used (i.e. "_\\data\\Example.classifier_"). For this example, the classifier is also added to a study that MUST already exist. The study pk is used to associate the classifier to the study.

Then you can run the following command.
```cmd
python .\examples\add_classifier.py <study_pk> <classifier_path>
```