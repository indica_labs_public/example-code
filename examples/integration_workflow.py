import asyncio
from gql import gql
from datetime import datetime
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session

STUDY_FIELD_NAME = "Sample Study Field"
IMAGE_FIELD_NAME = "Sample Image Field"


async def example(session):
    # -----------------
    # Query HALO Fields
    # -----------------
    study_field = await query_field(session, "SystemStudyFields", STUDY_FIELD_NAME)
    image_field = await query_field(session, "SystemImageFields", IMAGE_FIELD_NAME)

    # --------------------------
    # Execute Subscription Calls
    # --------------------------
    image_cataloged_task = asyncio.create_task(image_cataloged_subscription(session))
    barcode_scanned_task = asyncio.create_task(barcode_scanned_subscription(session, image_field))
    image_added_to_study_task = asyncio.create_task(image_added_to_study_subscription(session, study_field))
    await asyncio.gather(image_cataloged_task, barcode_scanned_task, image_added_to_study_task)

async def image_cataloged_subscription(session):
    # --------------------------------------------------
    # Create the Subscription and process event messages
    # --------------------------------------------------
    print(f"waiting for imageCatalogedEvent message...")
    async for message in session.subscribe(
        gql("""
            subscription {
              imageCatalogedEvent {
                imageId
                imageLocation
              }
            }""")
    ):
        # ---------------------
        # Process event message
        # ---------------------
        print(f' -> received: imageCatalogedEvent message \"{message["imageCatalogedEvent"]}\"')
        image_id = message["imageCatalogedEvent"]["imageId"]

        #-----------------------------------------------------------------------------------
        # Data from a 3rd party (i.e. LIMS provider) would be requested here to update HALO
        #-----------------------------------------------------------------------------------
        comment = f"Image was integrated at {datetime.now().strftime('%m/%d/%Y %H:%M:%S')}."
        await mutate_comment(session, comment, image_id) # Update image comment
        #-----------------------------------------------------------------------------------


async def barcode_scanned_subscription(session, image_field):
    # --------------------------------------------------
    # Create the Subscription and process event messages
    # --------------------------------------------------
    print(f"waiting for barcodeScannedEvent message...")
    async for message in session.subscribe(
        gql("""
            subscription {
              barcodeScannedEvent {
                imageId
              }
            }""")
    ):
        # ---------------------
        # Process event message
        # ---------------------
        print(f' -> received: barcodeScannedEvent message \"{message["barcodeScannedEvent"]}\"')

        #-----------------------------------------------------------------------------------
        # Data from a 3rd party (i.e. LIMS provider) would be requested here to update HALO
        #-----------------------------------------------------------------------------------
        image = await query_image(session, message["barcodeScannedEvent"]["imageId"])

        # Use the barcode to request data from 3rd party
        value = f'Image ({image["pk"]}) was integrated with 3rd party using barcode \"{image["barcode"]}\".'
        stain = "H&E"

        # Update image metadata from data requested form 3rd party
        await mutate_image_property(session, image, stain)
        await mutate_image_field(session, image, image_field, value)
        #-----------------------------------------------------------------------------------



async def image_added_to_study_subscription(session, study_field):
    # --------------------------------------------------
    # Create the Subscription and process event messages
    # --------------------------------------------------
    print(f"waiting for imageAddedToStudyEvent message...")
    async for message in session.subscribe(
        gql("""
            subscription {
              imageAddedToStudyEvent {
                studyId
                imageId
              }
            }""")
    ):
        # ---------------------
        # Process event message
        # ---------------------
        print(f' -> received: imageAddedToStudyEvent message \"{message["imageAddedToStudyEvent"]}\"')

        #-----------------------------------------------------------------------------------
        # Data from a 3rd party (i.e. LIMS provider) would be requested here to update HALO
        #-----------------------------------------------------------------------------------
        study = await query_study(session, message["imageAddedToStudyEvent"]["studyId"])
        image = await query_image(session, message["imageAddedToStudyEvent"]["imageId"])

        # Use the image file name to request data from 3rd party
        value = f'Study ({study["pk"]}) was integrated with 3rd party using image tag \"{image["tag"]}\".'

        # Update study metadata from data requested form 3rd party
        await mutate_study_field(session, study, study_field, value)
        #-----------------------------------------------------------------------------------


async def query_image(session, image_id):
    image = await session.execute(
        gql("""
            query($id: ID!) {
              imageById(id:$id) {
                id
                pk
                barcode
                tag
              }
            }
        """),
        variable_values={"id": image_id}
    )
    return image["imageById"]


async def query_study(session, study_id):
    study = await session.execute(
        gql("""
            query($id: ID!) {
              studyById(id: $id) {
                pk
                id
                name
              }
            }
        """),
        variable_values={"id": study_id}
    )
    return study["studyById"]


async def query_field(session, field_group, field_name):
    # -------------------------
    # Query Field By Group/Name
    # -------------------------
    fields = await session.execute(
        gql("""
            query($field_group: String!, $field_name: String!) {
              systemFieldGroups(
                query: {
                  where: {
                    q: [{ f: "groupIdentifier", pred: { op: EQ, v: $field_group } }]
                  }
                }
              ) {
                edges {
                  node {
                    groupIdentifier
                    entriesConnection(
                      first: -1
                      query: {
                        where: {
                          q: { f: "systemField.name", pred: { op: EQ, v: $field_name } }
                        }
                      }
                    ) {
                      edges {
                        node {
                          systemField {
                            id
                            type
                            name
                            caption
                            options
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
        """),
        variable_values={
          "field_group": field_group,
          "field_name": field_name
        }
    )
    if fields['systemFieldGroups']['edges'][0]['node']['entriesConnection']['edges']:
        field = fields['systemFieldGroups']['edges'][0]['node']['entriesConnection']['edges'][0]['node']['systemField']
        print(f"found field: {field['name']}")
        return field
    else:
        raise Exception(f"field not found: \"{field_name}\"")


async def mutate_image_property(session, image, stain):
    images = await session.execute(
        gql("""
            mutation($image_id: ID!, $stain: String!) {
              changeImageProperties(input: {
                imageId: $image_id
                stain: $stain
              }) {
                mutated {
                  node {
                    id
                    pk
                    stain
                    location
                  }
                }
              }
            }
        """),
        variable_values={
            "image_id": image["id"],
            "stain": stain
        }
    )
    print(f'updated image stain: image ({image["pk"]}) stain set to \"{images["changeImageProperties"]["mutated"][0]["node"]["stain"]}\" ')


async def mutate_comment(session, comment, image_id):
    comments = await session.execute(
        gql("""
            mutation($image_id: ID!, $comment: String!) {
              postImageComment(
                input: {
                  imageId: $image_id
                  body: $comment
                }
              ) {
                mutated {
                  node {
                    body
                  }
                }
              }
            }
        """),
        variable_values={
          "image_id": image_id,
          "comment": comment
        }
    )
    print(f'added comment: \"{comments["postImageComment"]["mutated"][0]["node"]["body"]}\" to image ({image_id})')


async def mutate_study_field(session, study, field, value):
    values = await session.execute(
        gql("""
            mutation($study_id: ID!, $field_id: ID!, $value: String!) {
              setStudyFieldValue(
                input: {
                  studyId: $study_id
                  systemFieldId: $field_id
                  value: $value
                }
              ) {
                mutated {
                  node {
                    id
                    pk
                    value
                  }
                }
              }
            }
        """),
        variable_values={
            "study_id": study["id"],
            "field_id": field["id"],
            "value": value
        }
    )
    print(f'added study metadata: \"{values["setStudyFieldValue"]["mutated"][0]["node"]["value"]}\" on study ({study["pk"]})')


async def mutate_image_field(session, image, field, value):
    values = await session.execute(
        gql("""
            mutation($image_id: ID!, $field_id: ID!, $value: String!) {
              updateImageFieldValues(
                input: {
                  imageId: $image_id
                  updates: [
                    { systemFieldId:$field_id, newValue: $value, operation: SET }
                  ]
                }
              ) {
                mutated {
                  node {
                    id
                    value
                  }
                }
              }
            }
        """),
        variable_values={
            "image_id": image["id"],
            "field_id": field["id"],
            "value": value,
            "operation": "SET"
        }
    )
    print(f'added image metadata: \"{values["updateImageFieldValues"]["mutated"][0]["node"]["value"]}\" on image ({image["pk"]})')


async def main():
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.ensure_future(main())
    loop = asyncio.get_event_loop()
    loop.run_forever()
