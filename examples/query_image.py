import asyncio
import sys
from gql import gql
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session


async def example(session, image_pk):
    # -----------------
    # Query Image By Pk
    # -----------------
    image = await session.execute(
        gql("""
            query test($pk: Int!) {
              imageByPk(pk:$pk) {
                id
                location
              }
            }
        """),
        variable_values={"pk": image_pk}
    )
    print(f"found image: {image['imageByPk']['location']} ({image['imageByPk']['id']})")


async def main(image_pk):
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session, image_pk)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(main(int(sys.argv[1])))
