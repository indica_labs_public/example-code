import asyncio
import sys
import xml.etree.ElementTree as ET
import base64
from gql import gql
from api.oidc import request_access_token
from api.client import create_client_session


async def example(session, study_pk, classifier_xml_path):
    # -----------------------
    # Add Classifier with XML
    # -----------------------
    
    # Parse XML File
    classifier_xml = ET.parse(classifier_xml_path).getroot()

    # Retrieve Architecture ID
    architecture_name = classifier_xml.find('ArchitectureName').text
    architecture_id = await get_architecture_id(session, architecture_name)
    
    # Retrieve Classifier Name
    name = classifier_xml.find('Name').text
    
    # Retrieve Classifier XML
    classifier_xml_str = str(ET.tostring(classifier_xml.find('XML').find('Classifier'), encoding='unicode'))
    
    # Retrieve Study ID from PK
    study_id = await get_study_id(session, study_pk)
    
    input_gql = {
        "name": name,
        "studyId": study_id,
        "architectureId": architecture_id,
        "classifierXml": classifier_xml_str,
        "isReadOnly": False,
        "isExportable": True
    }
    
    classifier = await session.execute(
        gql("""
            mutation($input: AddClassifierWithXmlInput!){
              addClassifierWithXml(input: $input) {
                mutated {
                  node {
                    pk
                    id
                    name
                  }
                }
              }
            }
            """),
        variable_values={"input": input_gql}
    )

    print(classifier)

async def get_architectures(session):
    # -------------------
    # Query Architectures
    # -------------------
    architectures = await session.execute(
        gql("""
            query {
                statisticalModelArchitectures {
                    edges{
                    node {
                        id
                        name
                    }
                    }
                }
            }
            """)
    )
    
    edges = architectures['statisticalModelArchitectures']['edges']
    
    return [edge['node'] for edge in edges]

async def get_architecture_id(session, architecture_name):
    # -----------------------------
    # Get Architecture ID from Name
    # -----------------------------
    architectures = await get_architectures(session)
    
    for architecture in architectures:
        if architecture['name'] == architecture_name:
            return architecture['id']
        
async def get_study_id(session, study_pk):
    # -----------------
    # Query Study By Pk
    # -----------------
    study = await session.execute(
        gql("""
            query($study_pk: Int!) {
              studyByPk(pk: $study_pk) {
                pk
                id
              }
            }
        """),
        variable_values={"study_pk": study_pk}
    )

    return study['studyByPk']['id']

async def main(study_pk, classifier_xml_path):
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session, study_pk, classifier_xml_path)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(main(int(sys.argv[1]), str(sys.argv[2])))
