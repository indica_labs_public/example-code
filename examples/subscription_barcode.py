import asyncio
from gql import gql
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session


async def example(session):
    # --------------------------------------------------
    # Create the Subscription and process event messages
    # --------------------------------------------------
    print(f"waiting for barcodeScannedEvent message...")
    async for message in session.subscribe(
        gql("""
            subscription {
              barcodeScannedEvent {
                imageId
              }
            }""")
    ):
        # ---------------------
        # Process event message
        # ---------------------
        image_id = message["barcodeScannedEvent"]["imageId"]
        image = await session.execute(
            gql("""
                query($id: ID!) {
                  imageById(id:$id) {
                    id
                    pk
                    barcode
                  }
                }
            """),
            variable_values={"id": image_id}
        )
        print(f' -> received: barcodeScannedEvent message \"{message["barcodeScannedEvent"]}\" -> new barcode for image:{image["imageById"]["pk"]} is \"{image["imageById"]["barcode"]}\"')


async def main():
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.ensure_future(main())
    loop = asyncio.get_event_loop()
    loop.run_forever()
