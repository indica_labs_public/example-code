import asyncio
import sys
from gql import gql
from examples.api.oidc import request_access_token
from examples.api.client import create_client_session


async def example(session, study_pk, image_path):
    # -----------------
    # Query Study By Pk
    # -----------------
    study = await session.execute(
        gql("""
            query($study_pk: Int!) {
              studyByPk(pk: $study_pk) {
                pk
                id
                name
              }
            }
        """),
        variable_values={"study_pk": study_pk}
    )
    print(f"found study: \"{study['studyByPk']['name']}\"")
    study_id = study['studyByPk']['id']

    # -----------------------
    # Catalog Image into HALO
    # -----------------------
    image = await session.execute(
        gql("""
            mutation($image_path: String!, $study_id: ID!) {
              catalogImage(
                input: {
                  location: $image_path
                  studyId: $study_id
                }
              ) {
                mutated {
                  node {
                    pk
                    id
                    location
                  }
                }
              }
            }
        """),
        variable_values={
            "study_id": study_id,
            "image_path": image_path
        }
    )
    print(f'cataloged image: \"{image["catalogImage"]["mutated"][0]["node"]["location"]}\" with id ({image["catalogImage"]["mutated"][0]["node"]["pk"]})')


async def main(study_pk, image_path):
    # --------------------------------------
    # Authenticate with OIDC and run Example
    # --------------------------------------
    token = await request_access_token()
    session = await create_client_session(token)
    await example(session, study_pk, image_path)
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(main(int(sys.argv[1]), str(sys.argv[2])))
