import asyncio
import sys
import base64
import pprint
import os.path
import xml.dom.minidom as xmlConverter
from gql import gql
from utils.parser import str_to_dict, dict_to_obj
from examples.api.client import create_client_session

output_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), '..\\output')

# Create output directory
if not os.path.isdir(output_path):
    os.mkdir(output_path)


async def example():
    # Deserialize input
    passed_input = sys.argv[1]
    obj_str = base64.b64decode(passed_input).decode("utf-8")
    base64_event_dict = str_to_dict(obj_str)
    base64_event_dict["message"] = passed_input
    base64_event_obj = dict_to_obj(base64_event_dict)

    # Authenticate with OIDC and get session
    session = await create_client_session(base64_event_obj.token, True)

    # Output external script message
    with open(f"{output_path}\\external-script-message.txt", "w") as the_file:
        the_file.write(pprint.pformat(base64_event_dict))

    # Query image data
    image = await session.execute(
        gql("""
            query($pk: Int!){
              imageByPk(pk: $pk){
                tag
                serializedAnnotations
              }
            }"""),
        variable_values={"pk": base64_event_obj.studyId}
    )

    # Export image annotation to .annotations file
    with open(os.path.join(output_path, (image["imageByPk"]["tag"] + ".annotations")), "w") as the_file:
        pretty_xml = xmlConverter.parseString(str(image["imageByPk"]["serializedAnnotations"])).toprettyxml()
        the_file.write(pretty_xml)

    # Close client session
    await session.client.close_async()


if __name__ == "__main__":
    asyncio.run(example())
