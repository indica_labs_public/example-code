import yaml
import os

from pathlib import Path

CONFIG_DIRECTORY = os.path.join(os.path.dirname(os.path.abspath(__file__)), "..\\config")


def get_settings():
    return yaml.safe_load(Path(f"{CONFIG_DIRECTORY}\\settings.yml").read_text())