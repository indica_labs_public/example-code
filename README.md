# Project Overview

Python example code for a GraphQL Client for HALO v4.0.

NOTE: For HALO v3.6 example code, go [here](https://gitlab.com/indica_labs_public/example-code/-/tree/release/3.6?ref_type=heads).

**All the example code that is provided is for demonstration purposes and not for production.**

This project is a collection of python code that uses the HALO GraphQL API. Examples that are covered include OIDC authentication (Authorization Code Flow & Client Credentials Flow), executing GraphQL queries, mutations and subscriptions.

OIDC authentication examples can be found here at [./examples/api](./examples/api).
- The [oidc.py](./examples/api/oidc.py) demonstrates authentication using Client Credentials Flow.
- The [local.py](./examples/api/local.py) demonstrates authentication using Authorization Code Flow.

Each example is defined in its own python file, which are listed below:
- [custom_analysis.py](./examples/custom_analysis.py)
- [deepzoom_tiles.py](./examples/deepzoom_tiles.py)
- [external_script.py](./examples/external_script.py)
- [mutation_pentagonannotation.py](./examples/mutation_pentagonannotation.py)
- [query_image.py](./examples/query_image.py)
- [subscription_barcode.py](./examples/subscription_barcode.py)
- [import_annotations.py](./examples/import_annotations.py)

NOTE: Valuable information about OIDC can be found [here](https://docs.authlib.org/en/latest/client/requests.html#requests-oauth-2-0).

## Instructions To Setup
### Step 1: Run setup script

- To setup the project, run the following batch [script](./setup.bat):
```cmd
setup.bat
```

This will create a virtual python environment and install all the needed packages for this project.

NOTE: The following project was created using **Python 3.10**.

### Step 2: Create Halo service client

Before any example code can be run, an oidc service client must be created.

This program uses the client credentials flow to acquire an access token from the identity provider by presenting a secret key (a "client secret"). You will need to configure the Indica Labs identity provider to add a custom client that is allowed to make API calls.

In a command prompt on the machine where the identity provider is installed (usually alongside the GraphQL API and the MySQL database), navigate to *'C:\Program Files\Indica Labs\Identity Provider'*, and run:

```cmd
IndicaLabs.ApplicationLayer.Halo.IdentityProvider.exe reconfigure --script AddResearchServiceClient "client_type=example;scopes=serviceuser|graphql"
```

This will add configuration for a new **client credentials** client and restart the identity provider service. If you then look in the configuration file *'C:\ProgramData\Indica Labs\Configuration\IndicaLabs.ApplicationLayer.Halo.IdentityProvider\local-production.yml'* you should see a new entry for the new client. An example modified section in this config file might look like the below yaml.

NOTE: The `secret` value provides _unrestricted access to the GraphQL API_ and should be protected as you would an admin password.

```yaml
# C:\ProgramData\Indica Labs\Configuration\IndicaLabs.ApplicationLayer.Halo.IdentityProvider\local-production.yml

identity_provider:
  clients:
  - !OidcClient/ClientCredentials
    id: example_WHERE-IS-WALDO
    scopes:
    - serviceuser
    - graphql
    require_client_secret: true
    secrets:
    - secret: qmwqQ69bzYpEwU8kXN5YUQ==
    implementation: !OidcImpl/HALO {}
```

It's ok to change the `id` to something more descriptive, but any manual change to this file will require restarting the "HALO Identity Provider" Windows service before it takes effect. With this configuration in place, the example code will be able to authenticate with the identity provider.

#### Next Step

Once a new **client credentials** client has been created, open [settings.yml](./config/settings.yml) and fill in the api connection settings. There is another README.md here at [api](./examples/api) that explains the authorization setup.

### Step 3: Run the python auth script

First make sure the virtual python environment is active.
- To activate, run the following:
```cmd
.\Scripts\activate.bat
```

Then run the example code with the following commands.
- This verifies the OIDC connection and returns an **auth token**:
```cmd
python main.py
```

### Step 4: Configure/Run example code

To configure and run the example code, review the README.md here [./examples](./examples).
