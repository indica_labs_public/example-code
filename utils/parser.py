from collections import namedtuple
import json
import unicodedata

def format_string(string):
    return unicodedata.normalize(
        'NFKD',
        string
    ).encode(
        'ascii',
        'ignore'
    )

def str_to_dict(string: str):
    parsed_string = format_string(string)
    return json.loads(parsed_string)

def dict_to_obj(dictionary: dict):
    parsed_string = format_string(json.dumps(dictionary).replace('"$', '"'))
    return json.loads(parsed_string, object_hook=lambda kwargs: namedtuple('item', kwargs.keys())(*kwargs.values()))
