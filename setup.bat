@echo off

echo "   Creating Virtual Environment..."
python -m pip install --upgrade pip
python -m pip uninstall -y virtualenv
python -m pip install virtualenv==20.14.0
python -m virtualenv .venv

echo "   Activating Virtual Environment..."
call .\.venv\Scripts\activate.bat

echo "   Installing Requirements..."
python -m pip install -r requirements.txt

echo "   Version Info..."
python -m pip --version
python --version
python -m pip list -v
call .\.venv\Scripts\deactivate.bat

echo "   Press enter to exit"
set /p input=
